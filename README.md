# English to German translator

This project implements a Transformer model for Neural Machine Translation (NMT) and server the Transformer model as a simple webb app using Python Flask. 
## Getting Started

In order to run the webb app with the Transformer model simply run the app.py file in the ./web_servive folder.

### Prerequisites

Find all prerequisites in the requirements.txt.

Main packages used are:

```
Python==3.6.7
Tensorflow==2.0 
Flask==1.1.1
```

### Installing

To train the Transformer model, with default parameter settings, navigate to the ./transformer directory excecute train.py in your enviroment. 

In terminal:
```
cd ~/english-to-german-translator/transformer
python train.py
```

Hyper parameters are configurable in train.py file.

To evaluate the model on examples sentences, run evaluation.py in the ./transformer directory. Insert test sentence directly at prediction call function at the end of the evaluation.py python file 

## Running the tests

Test of Transformer model:

```
Input: This is a translation from English to German
Output: das ist eine übersetzung von englisch
```


## Authors

* **Olle Holmberg**: https://www.linkedin.com/in/olle-holmberg-2ba23152/

## Acknowledgments

This project drew knowledge from many sources, a complete list can be found below:

* https://arxiv.org/abs/1706.03762
* https://arxiv.org/pdf/1503.04069.pdf
* https://towardsdatascience.com/word-level-english-to-marathi-neural-machine-translation-using-seq2seq-encoder-decoder-lstm-model-1a913f2dc4a7
* https://towardsdatascience.com/intuitive-understanding-of-attention-mechanism-in-deep-learning-6c9482aecf4f?source=user_profile
* https://machinelearningmastery.com/define-encoder-decoder-sequence-sequence-model-neural-machine-translation-keras/
* https://www.tensorflow.org/tutorials/text/transformer
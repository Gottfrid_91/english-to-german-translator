from __future__ import absolute_import, division, print_function, unicode_literals
import tensorflow as tf
from keras.callbacks import CSVLogger, ModelCheckpoint
from transformer_model import transformer
tf.random.set_seed(1234)
import tensorflow_datasets as tfds
import pickle

def loadData(file_):
    # for reading also binary mode is important
    dbfile = open(file_, 'rb')
    db = pickle.load(dbfile)
    dbfile.close()
    return db


questions_train = loadData("./data/X_train.pkl")
answers_train = loadData("./data/y_train.pkl")
questions_test = loadData("./data/X_test.pkl")
answers_test = loadData("./data/y_test.pkl")

questions = questions_train.values.tolist() + questions_test.values.tolist()
answers = answers_train.values.tolist() + answers_test.values.tolist()

# remove start and end tokens
answers = [answ.replace("START_ ", "").replace(" _END", "") for answ in answers]

print('Sample question: {}'.format(questions[20]))
print('Sample answer: {}'.format(answers[20]))

# Build tokenizer using tfds for both questions and answers
tokenizer = tfds.features.text.SubwordTextEncoder.build_from_corpus(
    questions + answers, target_vocab_size = 2 ** 13)

# Define start and end token to indicate the start and end of a sentence
START_TOKEN, END_TOKEN = [tokenizer.vocab_size], [tokenizer.vocab_size + 1]

# Vocabulary size plus start and end token
VOCAB_SIZE = tokenizer.vocab_size + 2

# Maximum sentence length
MAX_LENGTH = 40

# get number of words present
num_words = len(questions)

print("Total number of words in data set is:", num_words)

# Tokenize, filter and pad sentences
def tokenize_and_filter(inputs, outputs):
    tokenized_inputs, tokenized_outputs = [], []

    for (sentence1, sentence2) in zip(inputs, outputs):
        # tokenize sentence
        sentence1 = START_TOKEN + tokenizer.encode(sentence1) + END_TOKEN
        sentence2 = START_TOKEN + tokenizer.encode(sentence2) + END_TOKEN
        # check tokenized sentence max length
        if len(sentence1) <= MAX_LENGTH and len(sentence2) <= MAX_LENGTH:
            tokenized_inputs.append(sentence1)
            tokenized_outputs.append(sentence2)

    # pad tokenized sentences
    tokenized_inputs = tf.keras.preprocessing.sequence.pad_sequences(
        tokenized_inputs, maxlen = MAX_LENGTH, padding = 'post')
    tokenized_outputs = tf.keras.preprocessing.sequence.pad_sequences(
        tokenized_outputs, maxlen = MAX_LENGTH, padding = 'post')

    return tokenized_inputs, tokenized_outputs


questions, answers = tokenize_and_filter(questions, answers)

print('Vocab size: {}'.format(VOCAB_SIZE))
print('Number of samples: {}'.format(len(questions_train)))

BATCH_SIZE = 64
BUFFER_SIZE = 20000

# decoder inputs use the previous target as input
# remove START_TOKEN from targets
dataset = tf.data.Dataset.from_tensor_slices((
    {
        'inputs': questions,
        'dec_inputs': answers[:, :-1]
    },
    {
        'outputs': answers[:, 1:]
    },
))

dataset = dataset.cache()
dataset = dataset.shuffle(BUFFER_SIZE)
dataset = dataset.batch(BATCH_SIZE)
dataset = dataset.prefetch(tf.data.experimental.AUTOTUNE)


# Hyper-parameters
NUM_LAYERS = 2
D_MODEL = 256
NUM_HEADS = 8
UNITS = 512
DROPOUT = 0.1

model = transformer(
    vocab_size = VOCAB_SIZE,
    num_layers = NUM_LAYERS,
    units = UNITS,
    d_model = D_MODEL,
    num_heads = NUM_HEADS,
    dropout = DROPOUT)


def loss_function(y_true, y_pred):
    y_true = tf.reshape(y_true, shape = (-1, MAX_LENGTH - 1))

    loss = tf.keras.losses.SparseCategoricalCrossentropy(
        from_logits = True, reduction = 'none')(y_true, y_pred)

    mask = tf.cast(tf.not_equal(y_true, 0), tf.float32)
    loss = tf.multiply(loss, mask)

    return tf.reduce_mean(loss)


class CustomSchedule(tf.keras.optimizers.schedules.LearningRateSchedule):

    def __init__(self, d_model, warmup_steps=4000):
        super(CustomSchedule, self).__init__()

        self.d_model = d_model
        self.d_model = tf.cast(self.d_model, tf.float32)

        self.warmup_steps = warmup_steps

    def __call__(self, step):
        arg1 = tf.math.rsqrt(step)
        arg2 = step * (self.warmup_steps ** -1.5)

        return tf.math.rsqrt(self.d_model) * tf.math.minimum(arg1, arg2)


learning_rate = CustomSchedule(D_MODEL)

optimizer = tf.keras.optimizers.Adam(
    learning_rate, beta_1 = 0.9, beta_2 = 0.98, epsilon = 1e-9)


def accuracy(y_true, y_pred):
    # ensure labels have shape (batch_size, MAX_LENGTH - 1)
    y_true = tf.reshape(y_true, shape = (-1, MAX_LENGTH - 1))
    return tf.keras.metrics.sparse_categorical_accuracy(y_true, y_pred)

csv_logger = CSVLogger(filename = "./models" + '/history.csv',
                           append = True,
                           separator = ",")

checkpoint = ModelCheckpoint("./models" + "/model.h5",
                             monitor = 'loss',
                             verbose = 1,
                             save_weights_only = True,
                             save_best_only = True,
                             mode = 'min')

model.compile(optimizer = optimizer, loss = loss_function, metrics = [accuracy])

EPOCHS = 2

model.fit(dataset, epochs = EPOCHS,
          steps_per_epoch = int(num_words*0.9 // BATCH_SIZE),
          callbacks = [checkpoint, csv_logger])

import os
import requests
import sys
from flask import Flask, render_template, request
import evaluation
app = Flask(__name__)


@app.route('/', methods = ['GET', 'POST'])
def index():
    errors = []
    results = {}
    if request.method == "POST":
        # get url that the user has entered
        try:
            input = request.form['input_text']
            results = evaluation.predict(input)
        except:
            errors.append(
                "Unable to get URL. Please make sure it's valid and try again."
            )
    return render_template('index.html', output = results)


if __name__ == '__main__':
    app.run()

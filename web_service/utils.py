import re
import string
from pickle import load
from numpy import array
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.callbacks import ModelCheckpoint, TensorBoard, CSVLogger, LearningRateScheduler
from unicodedata import normalize


class Input():
    def __init__(self):
        pass

    def load_clean_sentences(self, filename):
        return load(open(filename, 'rb'))

    # fit a tokenizer
    def create_tokenizer(self, lines):
        tokenizer = Tokenizer()
        tokenizer.fit_on_texts(lines)
        return tokenizer

    def clean_sentence(self, sentence):
        # prepare regex for char filtering
        re_print = re.compile('[^%s]' % re.escape(string.printable))
        # prepare translation table for removing punctuation
        table = str.maketrans('', '', string.punctuation)
        # normalize unicode characters
        line = normalize('NFD', sentence).encode('ascii', 'ignore')
        line = line.decode('UTF-8')
        # tokenize on white space
        line = line.split()
        # convert to lowercase
        line = [word.lower() for word in line]
        # remove punctuation from each token
        line = [word.translate(table) for word in line]
        # remove non-printable chars form each token
        line = [re_print.sub('', w) for w in line]
        # remove tokens with numbers in them
        line = [word for word in line if word.isalpha()]
        return line

    # max sentence length
    def max_length(self, lines):
        return max(len(line.split()) for line in lines)

    # encode and pad sequences
    def encode_sequences(self, tokenizer, length, lines):
        # integer encode sequences
        X = tokenizer.texts_to_sequences(lines)
        # pad sequences with 0 values
        X = pad_sequences(X, maxlen = length, padding = 'post')
        return X

    # one hot encode target sequence
    def encode_output(self, sequences, vocab_size):
        ylist = list()
        for sequence in sequences:
            encoded = to_categorical(sequence, num_classes = vocab_size)
            ylist.append(encoded)
        y = array(ylist)
        y = y.reshape(sequences.shape[0], sequences.shape[1], vocab_size)
        return y


class Train():
    def __init__(self, training_params):
        self.training_params = training_params
        pass

    def step_decay(self, epoch):
        """Learning Rate Schedule

        Learning rate is scheduled to be reduced after 80, 120, 160, 180 epochs.
        Called automatically every epoch as part of callbacks during training.

        # Arguments
            epoch (int): The number of epochs

        # Returns
            lr (float32): learning rate
        """
        lr = self.training_params["learning_rate"]
        if epoch >= int(self.training_params["epochs"] / 3):
            lr *= 5e-1
        if epoch >= int(self.training_params["epochs"] / 2):
            lr *= 2e-1
        if epoch >= int(self.training_params["epochs"] / 3) * 2:
            lr *= 5e-1
        if epoch >= int((self.training_params["epochs"] / 3) * 2.25):
            lr *= 2e-1
        print('Learning rate: ', lr)

        return lr

    def callbacks(self):
        '''callbacks'''
        lr_scheduler = LearningRateScheduler(self.step_decay)

        csv_logger = CSVLogger(filename = self.training_params["model_dir"] + '/history.csv',
                               append = True,
                               separator = ",")

        checkpoint = ModelCheckpoint(self.training_params["model_dir"] + "/nmt_wights.h5",
                                     monitor = 'val_loss',
                                     verbose = 1,
                                     save_best_only = True,
                                     mode = 'min')

        tb = TensorBoard(log_dir = self.training_params["model_dir"] + "/tensorboard",
                         histogram_freq = 0,
                         write_graph = True,
                         write_images = True,
                         embeddings_freq = 0,
                         embeddings_layer_names = None,
                         embeddings_metadata = None)

        cb = [lr_scheduler, csv_logger, checkpoint, tb]
        return cb

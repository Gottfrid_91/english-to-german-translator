import tensorflow.compat.v1 as tf
from basic_lstm.models import functional_model
tf.disable_v2_behavior()
import os
import pickle
from tensorflow.keras.optimizers import Adam
from utils import Train, Inputs
import sys

ROOT_DIR = os.path.dirname(os.path.dirname(__file__))
PROJECT_DIR = os.path.join(ROOT_DIR, "english-to-german-translator")

# set training parameters
training_params = {}
training_params["latent_dim"] = 128
training_params["batch_size"] = 128
training_params["epochs"] = 100
training_params["learning_rate"] =  1e-2
training_params["lsmt_layers"] = 1
training_params["model_dir"] = "./models"

# load utils classes
train_ops = Train(training_params = training_params)
input = Inputs()

# load datasets
dataset = input.load_clean_sentences(os.path.join(sys.path[0], 'data/english-german-both.pkl'))
train = input.load_clean_sentences(os.path.join(sys.path[0], 'data/english-german-train.pkl'))
test = input.load_clean_sentences(os.path.join(sys.path[0], 'data/english-german-test.pkl'))

# prepare english tokenizer
eng_tokenizer = input.create_tokenizer(dataset[:, 0])
eng_vocab_size = len(eng_tokenizer.word_index) + 1
eng_length = input.max_length(dataset[:, 0])
print('English Vocabulary Size: %d' % eng_vocab_size)
print('English Max Length: %d' % (eng_length))
# prepare german tokenizer
ger_tokenizer = input.create_tokenizer(dataset[:, 1])
ger_vocab_size = len(ger_tokenizer.word_index) + 1
ger_length = input.max_length(dataset[:, 1])
print('German Vocabulary Size: %d' % ger_vocab_size)
print('German Max Length: %d' % (ger_length))

# prepare training data
trainy = input.encode_sequences(ger_tokenizer, ger_length, train[:, 1])
trainX = input.encode_sequences(eng_tokenizer, eng_length, train[:, 0])
trainY = input.encode_output(trainy, ger_vocab_size)

# prepare validation data
testy = input.encode_sequences(ger_tokenizer, ger_length, test[:, 1])
testX = input.encode_sequences(eng_tokenizer, eng_length, test[:, 0])
testY = input.encode_output(testy, ger_vocab_size)

# create model directordy
if not os.path.exists(training_params["model_dir"]):
    os.makedirs(training_params["model_dir"])
    os.makedirs(training_params["model_dir"]+"/tensorboard")

# save config file into model dir
dbfile = open(os.path.join(training_params["model_dir"], 'training_params.pkl'), 'ab')
# source, destination
pickle.dump(training_params, dbfile)
dbfile.close()

# define model
model = functional_model(eng_vocab_size, ger_vocab_size, eng_length, ger_length,
                                   training_params)

model.compile(optimizer = Adam(lr=training_params["learning_rate"]),
              loss = 'categorical_crossentropy',
              metrics=["accuracy"])

# summarize defined model
print(model.summary())

# fit model
model.fit([trainX, trainy], trainY, epochs = training_params["epochs"],
          batch_size = training_params["batch_size"],
          validation_data = ([testX, testy], testY),
          callbacks = train_ops.callbacks(),
          verbose = 1)

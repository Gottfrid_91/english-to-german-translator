import tensorflow.compat.v1 as tf
from tensorflow.keras.backend import concatenate
tf.disable_v2_behavior()
from tensorflow.keras import Input, Model
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Embedding
from tensorflow.keras.layers import RepeatVector
from tensorflow.keras.layers import TimeDistributed


# define NMT model
def sequential_model(src_vocab, tar_vocab, src_timesteps, tar_timesteps, n_units):
    model = Sequential()
    model.add(Embedding(src_vocab, n_units, input_length = src_timesteps, mask_zero = True))
    model.add(LSTM(n_units))
    model.add(RepeatVector(tar_timesteps))
    model.add(LSTM(n_units, return_sequences = True))
    model.add(TimeDistributed(Dense(tar_vocab, activation = 'softmax')))
    return model


def functional_model(src_vocab, tar_vocab, src_timesteps, tar_timesteps, training_params):
    input = Input(shape = (src_timesteps,))
    emb = Embedding(src_vocab, training_params["latent_dim"], input_length = src_timesteps, mask_zero = True)(input)

    enc = LSTM(training_params["latent_dim"])(emb)
    enc = RepeatVector(tar_timesteps)(enc)

    for i in range(1, training_params["lsmt_layers"]):
        enc = LSTM(training_params["latent_dim"])(enc)
        enc = RepeatVector(tar_timesteps)(enc)

    dec = LSTM(training_params["latent_dim"], return_sequences = True)(enc)

    for i in range(1, training_params["lsmt_layers"]):
        dec = LSTM(training_params["latent_dim"], return_sequences = True)(dec)

    dec = TimeDistributed(Dense(tar_vocab, activation = 'softmax'))(dec)
    return Model([input], dec)